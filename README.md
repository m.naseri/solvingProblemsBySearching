# Implementing Search Algorithms in AI (Spring 2019)
- Written in Python from scratch, Artificial Intelligence course project, Dr. Ahmad Nickabadi
- Including BFS, DFS, DLS, IDDFS, Bidirectional, UCS, AStar Algorithms for solving 8 puzzle problem